# Commerce offline payments mini panels

This Drupal 7 Commerce module allows you to define mini panels that can be used
as offline payment methods for commerce payment module.

### Installation
Install as usual

### Usage
Define mini panels and set as category 'Payment method'. This makes hte mini
panel selectable as payment method. Go to admin/commerce/config/payment-methods
and activate the new payment method. Maybe a clear cache is required before the
payment is visible under admin/commerce/config/payment-methods.